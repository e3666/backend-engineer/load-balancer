import os
from flask import Flask

port = os.environ['F_PORT']

app = Flask(__name__)

@app.route('/')
def main():
    return f'servindo na porta {port}'

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=port)