# Sumário

- [**Load Balancer**](#load-balancer)
- [**Load Balancer Layer 4**](#load-balancer-layer-4)
    - [Prós](#prós)
    - [Contras](#contras)
    - [Exemplo](#exemplo)
- [**Load Balancer Layer 7**](#load-balancer-layer-7)
    - [Prós](#prós)
    - [Contras](#contras)
    - [Exemplo](#exemplo)


# Load Balancer

Load balancers são proxys reversas, e o que eles fazem é simplesmente conectar um client a um servidor, dentre os servidores que estão disponíveis, assim balanceando a carta.

Mas Temos dois tipos de Load Balancers, o de layer 4 e o de layer 7.

> OBS.: Se quiser entender mais sobre esse os layers da web, veja o vídeo [The OSI Model - Explained by Example](https://www.youtube.com/watch?v=7IS7gigunyI)


Em resumo, o layer 4 está na camada de transporte, já o layer 7 está na camada de aplicação. Sendo assim o layer 4 não conheçe o real conteúdo da mensagem, ele só sabe que algo tem que ir do ponto A para B, já o layer 7 possui informações como headers, cookies, body, enfim, tudo (mas claro, tudo criptografado).

# Load Balancer Layer 4

Como o layer 4 não sabe nada do conteúdo da mensagem, ele simplesmente pega o pacote, e manda para um servidor a sua disposição:

<img src='./images/fig_01.svg'/>

Pela imagem, podemos perceber que a ideia é a request sair do ip 1.1.1.1 ir para o ip 1.1.1.2 e esse ip simplesmente joga para outro ip. Então tudo fica no nível de comunicação, é tudo com relação aos endereços.

## Prós

- É mais simples
- É mais seguro, pois o load balancer não vai ver o conteúdo
- Existe apenas uma conexão TCP

## Contras

- O balanceamento não é muito inteligente, pois como não se pode ver o conteúdo da request não tem como saber qual tarefa será executada, logo não é possível destinar para um server mais preparádo para essa tarefa.
- Não aplicável a micro-serviços, pois eles se baseam na rota, ou seja, /images, /pdf, e com esse load balancer nós não sabemos para onde a request vai, pois não temos os headers.
- O load balancer não pode fazer cache, pois ele não tem informações suficientes para isso.


## Exemplo

Para o teste, irei criar uma aplicação python com flask que roda irá rodar na porta 3000, ou 5000 de acordo com a variável ambiente `F_PORT`. E para o load balancer, irei utilizar o HAProxy.

Para facilitar o teste, irei fazer tudo via docker. Os arquivos estão na pasta [L4](./L4).

O arquivo do docker-compose é esse abaixo, o que importa de vermos é que dei os nomes dos containers do server python, de server1 e server2, e em cada caso eu estou passando uma variável de ambiente `F_PORT` diferente. Assim o server1 está na 3000 e o server2 na 5000.

- `docker-compose.yml`
```yml
version: "3"

services: 
    haproxy:
        image: haproxy
        ports: 
            - "8888:8888"
        volumes: 
            - "./tcp.cfg:/usr/local/etc/haproxy/tcp.cfg"
        command: "haproxy -f /usr/local/etc/haproxy/tcp.cfg"

    server1:
        build: .
        ports: 
            - "3000:3000"
        environment: 
            F_PORT: 3000
        command: "python server.py"

    server2:
        build: .
        ports: 
            - "5000:5000"
        environment: 
            F_PORT: 5000
        command: "python server.py"
```

Já o arquivo de configurações do HAproxy, será o arquivo `tcp.cfg` que está abaixo, e dele temos alguns pontos a serem observados

- Em defaults:
    - `mode tcp` isso indica que é um load balancer de layer 4.
- Em frontend:
    - O frontend é o canal de entrada, ou seja, o "lado virado" para o client, que nesse caso irá ficar disponível na porta 8888
    - default_backend: Indica a "saída" do load balancer, sendo que isso irá apontar para um backend chamado nodes
- Em Backend:
    - O nome do backend é nodes, que é o indicado no frontend
    - Aqui foram listados cada server que serão roteados os dados
    - Veja que o host do server é o mesmo nome que dei para os containers no arquivo `docker-compose.yml`, eu preciso fazer isso pois os containers possuem uma rede interna, e assim fica bem mais simples de referenciar entre eles. E eu aproveitei os nomes dos containers para dar o mesmo nome na configuração, então fique ciente que não é obrigatório colocar o mesmo nome ali, eu poderia ter feito: `server serverX server1:3000 check`


- `tcp.cfg`:
```cfg
    global
        maxconn 4096

    defaults
        log     global
        mode    tcp
        timeout connect   5000
        timeout client   50000
        timeout server   50000

    frontend localnodes
        bind *:8888
        default_backend nodes

    backend nodes
        server server1 server1:3000 check
        server server2 server2:5000 check
```

Para executar:

```sh
$ cd L4
$ docker-compose up -d
```

Agora vá para o browser e jogue a url: `http://localhost:8888`, fique recarregando e veja que em diferentes momentos o load balancer aponta para o server da porta 3000 e outros para o server da porta 5000. Experimente também derrubar um dos containers, e veja que agora ele sempre apontará para o outro.


<img src='./images/fig_02.gif'/>


# Load Balancer Layer 7

Já o load balancer de Layer 7 consegue ver o que está passando por ele, logo a distinção de rotas não é mais por IP, e sim por rotas.

<img src='./images/fig_03.svg'/>

Pela imagem, vemos que se a rota for `/images` o load balancer destinará para o server `2.2.2.2` já se a rota for `/pdf` irá para 2.2.2.3

## Prós

- Balanceamento inteligênte, pois agora destinamos cada tarefa para seu devido lugar
- É possível efetuar cache
- Perfeito para micro-serviços

## Contras

- O load balancer tem acesso ao dados
- Precisa de duas conexões TCP

## Exemplo

O exemplo será montado exatamente como o exemplo do layer 4, utilizando os mesmos servidores flask, e também o HAProxy. porém agora irei fazer algumas modificações no arquivo de configurações da proxy, que agora se chamará `http.cfg`.

Também farei o teste via docker. Os arquivos estão na pasta [L7](./L7).

O arquivo de configurações do HAProxy, pode ser visto abaixo, e veja que ele é bem mais complicado para o layer 7. Alguns pontos de destaque:

- Em defaults:
    - `mode http`, isso aqui que faz ser um load balancer de layer 7
- Em frontend:
    - Continuaremos monitorando na porta 8888
    - o `mode http` também precisa estar aqui
    - Definimos uma `acl` e o caminho para o qual ela se refere, ou seja, temos a `acl` `app_images` se referindo ao caminho `/images`
    - diferente do layer 4, temos que definir um backend para cada caso. Veja que eu precisei especificar qual backend seria utilizado caso o caminho utilizado fosse o da `acl` `app_images` e o da `acl` `app_pdf`
- Em backend
    - Temos dois backends agora e nos dois eu preciso colocar `mode http`, e aqui eu gostaria de deixar uma observação, em cada um desses backends você pode colar quantos servers quiser. Ou seja, se você colocar dois servers no backend de imagens, o load balancer irá redirecionar os clients da rota `/images` entre esses dois servidores, e isso é ótimo para escalabilidade. Afinal se um dos seus serviços é muito mais pesado que outro, você pode destinar mais servers para ele.

- `http.cfg`
```cfg
    global
        maxconn 4096

    defaults
        log     global
        mode    http
        option  httplog
        option  dontlognull
        retries 3
        maxconn 2000
        timeout connect   5000
        timeout client   50000
        timeout server   50000

    frontend localnodes
        bind *:8888
        mode http
        acl app_images path_end -i /images
        acl app_pdf path_end -i /pdf
        use_backend images_server if app_images
        use_backend pdf_server if app_pdf

    backend images_server
        mode http
        server server1 server1:3000 check
    
    backend pdf_server
        mode http
        server server2 server2:5000 check
```

Outra coisa importante, é que o servidor que receberá uma determinada rota, precisa ter ela implementada. Ou seja, a proxy irá passa o path para o server (pelo menos o HAProxy). Logo se o server não tiver esse path implementado, nada vai funcionar. Por isso eu fiz uma modificação no meu arquivo python, para que ele contenha as rotas `/pdf` e `/images`.

> OBS.: O arquivo precisou conter as duas rotas pois eu estou utilizando o mesmo arquivo para os dois servidores.

- `server.py`
```py
import os
from flask import Flask

port = os.environ['F_PORT']

app = Flask(__name__)

@app.route('/images')
def img():
    return f'servindo na porta {port}'

@app.route('/pdf')
def pdf():
    return f'servindo na porta {port}'

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=port)
```


Agora derrube os containers do teste L4

```sh
$ cd L4
$ docker-compose down
```

Agora execute o comando:

```sh
$ cd L7
$ docker-compose up -d
```

<img src='./images/fig_04.gif'/>
